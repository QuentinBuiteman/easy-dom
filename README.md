# EasyDOM
Extend DOMDocument with some easy-of-use functionality.

## Installation
```
composer require quentinbuiteman/easy-dom
```

## Usage
```php
use \EasyDOM\DOMDocument;

// Initliaze new DOMDocument with preserveWhiteSpace = false and formatOutput = true
$DOM = new DOMDocument();

/**
 * Example to create a new element
 *
 * First parameter determines which element to create
 * Second parameter is an array of attributes to use in element creation (optional)
 * Third parameter is the text for inside the element (optional)
 */
$atts = array('class' => 'text');
$p = $this->DOM->generateElement('p', $atts, 'This will be the text inside the paragraph.');
$this->DOM->appendChild($p);

/**
 * Example to add nodes to an element based on a HTML string
 */
$html = '<p>This is a test HTML string.</p>';
$div = $this->DOM->createElement('div');
$div->addInnerHTML($html);
```
