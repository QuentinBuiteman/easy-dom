<?php
/**
 * Extend DOMDocument for standard init and custom functions
 *
 * PHP version 7
 *
 * @package EasyDOM\DOMDocument
 * @author  Quentin Buiteman <hello@quentinbuiteman.com>
 * @license https://opensource.org/licenses/MIT MIT
 */

namespace EasyDOM;

class DOMDocument extends \DOMDocument
{
    /**
     * Run construct and set preserveWhiteSpace and formatOutput
     */
    public function __construct()
    {
        parent::__construct();

        $this->preserveWhiteSpace = false;
        $this->formatOutput = true;
        $this->registerNodeClass('DOMElement', 'EasyDOM\DOMHTMLElement');
    }

    /**
     * Wrapper for createElement
     *
     * @param string $tag  Tag of element to create
     * @param array  $atts Attributes to add to element
     * @param string $text Text to add inside element
     *
     * @return DOMElement
     */
    public function generateElement(string $tag, array $atts = null, string $text = null): \DOMElement
    {
        $elem = $this->createElement($tag);

        // Add attributes if atts param has been set
        if ($atts) {
            foreach ($atts as $att => $value) {
                $elem->setAttribute($att, $value);
            }
        }

        // Add text node if text param has been set
        if ($text) {
            $text_node = $this->createTextNode($text);
            $elem->appendChild($text_node);
        }

        return $elem;
    }
}
