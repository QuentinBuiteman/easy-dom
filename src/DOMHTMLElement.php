<?php
/**
 * Extend DOMElement with custom functions
 *
 * PHP version 7
 *
 * @package EasyDOM\DOMHTMLElement
 * @author  Quentin Buiteman <hello@quentinbuiteman.com>
 * @license https://opensource.org/licenses/MIT MIT
 */

namespace EasyDOM;

class DOMHTMLElement extends \DOMElement
{
    /**
     * Run construct and set preserveWhiteSpace and formatOutput
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Translate HTML string to DOM nodes to insert into element
     *
     * @param string $html HTML to translate
     *
     * @return void
     */
    public function addInnerHTML($html)
    {
        // New DOM to translate file svg into DOM
        $DOM = new \DOMDocument();
        libxml_use_internal_errors(true);
        $DOM->loadHTML('<div id="container">' . $html . '</div>');

        // Get svg from DOM, import into original class DOM and append
        $container = $DOM->getElementById('container');
        foreach ($container->childNodes as $node) {
            $node = $this->ownerDocument->importNode($node, true);
            $this->appendChild($node);
        }
    }
}
